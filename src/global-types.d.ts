declare type Todo = {
    id : number
    title : string
    completed : boolean
}

declare type TAction = {
    type : unknown
    payload : unknown
}