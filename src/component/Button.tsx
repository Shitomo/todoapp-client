import React from 'react';
import styled from 'styled-components';

interface Props {
    children? : React.ReactNode
    onClick? : any
}

const Button = (props : Props) => {
    return (
        <DesignConfig>
           <span className="add-todo" onClick={props.onClick}>
                {props.children}
            </span>
        </DesignConfig>
    );
}

const DesignConfig = styled.header`
border-width: 0;
padding-top: 0.5rem;
padding-bottom: 0.5rem;
padding-left: 0.75rem;
padding-right: 0.75rem;
background-color: transparent;
line-height: 1rem;
color: #303030;
fill: currentColor;
box-shadow: inset 0 0 0 1px #bfbfbf;
justify-content: center;
align-items: center;
font-size: 0.875rem;
border-radius: 0.25rem;
    `

export default Button;