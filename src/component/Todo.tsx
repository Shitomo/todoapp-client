import React from "react";
import { connect } from 'react-redux';
import { toggleTodo } from '../feature/actions';

interface MapStateProps {
    todo : Todo
}

interface MapDispatchProps {
  toggleTodo : (i : number) => void
}

export const Todo = (props : MapStateProps & MapDispatchProps) => (
  <li
    className="todo-item"
    onClick={() => {props.toggleTodo(props.todo.id)}}
  >
    {props.todo && props.todo.completed ? "👌" : "👋"}{" "}
    <span
      className={
        "todo-item__text " + props.todo.completed? "todo-item__text--completed" : ""
      }
    >
      {props.todo.title}
    </span>
  </li>
);

/**
 * connectがやっていること
 * toggleTodoを受け取る
 * 内部で受け取った引数を基に，toggleTodoを実行しtoggleTodoActionを生成，それをdispachする関数を作成
 * その関数をTodoコンポーネントに渡す
**/
export default connect(null, { toggleTodo })(Todo);