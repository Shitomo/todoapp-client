import React from "react";
import { connect } from 'react-redux';
import { getTodosByVisibilityFilter } from '../feature/selectors';
import {StateType} from '../feature/store';
import Todo from './Todo';

interface MapStateProps {
    todos : Todo[]
}

const TodoList = ( props : MapStateProps ) => (
  <ul className="todo-list">
    {props.todos && props.todos.length
      ? props.todos.map((todo, index) => {
          return <Todo key={`todo-${todo.id}`} todo={todo} />;
        })
      : "No todos, yay!"}
  </ul>
);

const mapStateToProps = (state : StateType) => {
    const { visibilityFilter } = state
    const todos = getTodosByVisibilityFilter(state, visibilityFilter)
    return { todos }
};

export default connect(mapStateToProps)(TodoList);