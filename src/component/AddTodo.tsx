import React, {useState} from "react";
import { connect } from 'react-redux';
import { addTodo } from '../feature/actions';
import Button from './Button';

interface MapDispatchProps {
    addTodo : (title : string) => void
}

export const AddTodo = (props : MapDispatchProps) => {
    const [input, setInput] = useState<string>("");

    const updateInput = (input : string) => {
        setInput(input);
    };

    const handleAddTodo = () => {
        // dispatches actions to add todo
        props.addTodo(input)
        // sets state back to empty string
        setInput("")
    };
    return (
        <div>
        <input
            onChange={e => updateInput(e.target.value)}
            value={input}
        />
        <Button onClick={handleAddTodo}>
            Add Todo
        </Button>
        </div>
    );
}

export default connect(null, { addTodo })(AddTodo)