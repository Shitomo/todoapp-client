import React from "react";
import { connect } from 'react-redux';
import { setFilter } from '../feature/actions';
import { StateType } from '../feature/store'
import { VISIBILITY_FILTERS } from "../constants";


interface MapDispatchProps {
    setFilter : (filter : string) => void
}

interface MapStateProps {
    activeFilter : string
}

const VisibilityFilters = (props : MapDispatchProps & MapStateProps) => {
  return (
    <div className="visibility-filters">
      {Object.values(VISIBILITY_FILTERS).map(filterKey => {
        const currentFilter = filterKey;
        return (
          <span
            key={`visibility-filter-${currentFilter}`}
            className={
              "filter " + (currentFilter === props.activeFilter ? "filter--active" : "")
            }
            onClick={() => {props.setFilter(currentFilter)}}
          >
            {currentFilter}
          </span>
        );
      })}
    </div>
  );
};

const mapStateToProps = (state : StateType) => {
    return { activeFilter: state.visibilityFilter };
  };

export default connect(mapStateToProps,{ setFilter })(VisibilityFilters);