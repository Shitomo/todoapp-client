import { ADD_TODO, TOGGLE_TODO } from "../actionTypes";
import { TodoAction } from "../actions"

interface todoState {
    allIds : number[]
    byIds :  {
      [id : number] : Todo
    }
}

const initialState : todoState = {
  allIds: [],
  byIds: {}
};

const todoReducer = (state = initialState, action : TodoAction) =>  {
  switch (action.type) {
    case ADD_TODO: {
      const { todoId, todoTitle } = action.payload;
      return {
        ...state,
        allIds: [...state.allIds, todoId],
        byIds: {
          ...state.byIds,
          [todoId]: {
            id : todoId,
            title : todoTitle,
            completed: false
          }
        }
      };
    }
    case TOGGLE_TODO: {
      // TODO: TOGGLE_TODOの時にはtodoTitleがないので，取得しようとした際に型エラーを出せたらいいな
      const { todoId } = action.payload;
      return {
        ...state,
        byIds: {
          ...state.byIds,
          [todoId]: {
            ...state.byIds[todoId],
            completed: !state.byIds[todoId].completed
          }
        }
      };
    }
    default:
      return state;
  }
}

export default todoReducer;