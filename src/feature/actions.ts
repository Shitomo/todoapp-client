import { ADD_TODO, TOGGLE_TODO, SET_FILTER } from "./actionTypes";


export interface TodoAction {
    type : string
    payload : {
        todoId : number
        todoTitle : string

    }
}

export interface FilterAction {
    type : string
    payload : {
      filter : string
    }
}

let nextTodoId = 0;

export const addTodo : (content : string) => TodoAction = (content : string) => ({
  type: ADD_TODO,
  payload: {
    todoId : ++nextTodoId,
    todoTitle : content
  }
});

export const toggleTodo : (id : number) => TodoAction = (id : number) => ({
  type: TOGGLE_TODO,
  payload: { 
      todoId : id,
      todoTitle : ""//使われないが，TodoActionの型で扱いたいので，とりあえず空文字を設定
  }
});

export const setFilter : (filter : string) => FilterAction  = (filter : string) => ({ type: SET_FILTER, payload: { filter } });
