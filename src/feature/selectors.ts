import { StateType } from './store';
import { VISIBILITY_FILTERS } from "../constants";

export const getTodoList : (state : StateType) => number[] = (state :  StateType ) =>
  state.todos ? state.todos.allIds : [];

export const getTodoById : (state : StateType, id : number) => Todo = (state :  StateType , id : number) =>
  state.todos.byIds
    ? { ...state.todos.byIds[id], id }
    : {
      id : -1,
      title : "",
      completed : false
    };

/**
 * example of a slightly more complex selector
 * select from store combining information from multiple reducers
 */
export const getTodos : (state : StateType) => Todo[]   = (state :  StateType ) =>
  getTodoList(state).map(id => getTodoById(state, id));


export const getTodosByVisibilityFilter = (store : StateType, visibilityFilter : string) => {
  const allTodos = getTodos(store)
  switch (visibilityFilter) {
    case VISIBILITY_FILTERS.COMPLETED:
      return allTodos.filter((todo) => todo?.completed)
    case VISIBILITY_FILTERS.INCOMPLETE:
      return allTodos.filter((todo) => !todo.completed)
    case VISIBILITY_FILTERS.ALL:
    default:
      return allTodos
  }
}