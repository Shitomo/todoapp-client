import { SET_FILTER } from "../actionTypes";
import { VISIBILITY_FILTERS } from "../../constants";
import { FilterAction } from "../actions"

const initialState = VISIBILITY_FILTERS.ALL;

const visibilityFilterReducer = (state = initialState, action : FilterAction) => {
  switch (action.type) {
    case SET_FILTER: {
      return action.payload.filter;
    }
    default: {
      return state;
    }
  }
};

export default visibilityFilterReducer;